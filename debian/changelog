yale (5.0.95-4) UNRELEASED; urgency=medium

  * Trim trailing whitespace.
  * Update watch file format version to 4.

 -- Debian Janitor <janitor@jelmer.uk>  Wed, 20 Jul 2022 19:19:28 -0000

yale (5.0.95-3) unstable; urgency=medium

  * debian/control:
    - Updated Standards version
    - Remove retired comaintainer (Closes: #869335)
    - Add VCS information pointing to new Salsa repository
    - Update to debian compatibility version 13, remove debian/compat
  * debian/rules:
    - Simplify to use debhelper (Closes: #999125)
  * debian/install: Create to install the files
  * debian/source/format: Declare version 3.0 (no patches currently for this
     package)

 -- Javier Fernández-Sanguino Peña <jfs@debian.org>  Sun, 27 Mar 2022 21:44:00 +0200

yale (5.0.95-2) unstable; urgency=low

  * Updated to Debian policy 3.8.4
  * Added ${misc:Depends} depends to binary package, required by debhelper.

 -- Francisco Manuel Garcia Claramonte <francisco@debian.org>  Sun, 28 Feb 2010 11:41:19 +0100

yale (5.0.95-1) unstable; urgency=low

  * New upstream release
  * Updated to Debian policy 3.8.3
  * Updated Maintainer control field to @debian.org.
  * Added Homepage control field.
  * Changed GPL reference to GPL-2 in debian/copyright.
  * Added debian/watch file.
  * Cleaned up debian/rules file.

 -- Francisco Manuel Garcia Claramonte <francisco@debian.org>  Thu, 01 Oct 2009 11:50:41 +0200

yale (1.0-15) unstable; urgency=low

  * Updated to Debian Policy 3.8.0.1
  * Updated depends control field from debhelper version 5 to 7.
  * Modified debian/compat debhelper compatibility file to 7.
  * Changed debian/copyright to fix the Makefile and .spec license.
  * Fixed the debian/postinst and debian/prerm scripts according to
    Debian Policy.
  * Changed debian/rules install target to avoid a lintian warning.

 -- Francisco Garcia <franciscomanuel.garcia@hispalinux.es>  Mon, 09 Feb 2009 11:19:01 +0100

yale (1.0-14) unstable; urgency=low

  * No longer install yale.spec; a newer version is installed elsewhere
    (and has been for some time) by the starplot package.

 -- Kevin B. McCarty <kmccarty@debian.org>  Fri, 11 Apr 2008 09:06:21 -0700

yale (1.0-13) unstable; urgency=low

  * Updated to Debian Policy 3.7.3
  * Changed the short description control field to make it more
    clear. (Closes: #464434)
  * Modified the Build-Depends-Indep to Build-Depends control field.
  * Modified debian/rules clean process to catch error messages from
    the clean or distclean rules.
  * Added debian/compat file with compatibility version 5.

 -- Francisco Garcia <franciscomanuel.garcia@hispalinux.es>  Sun, 10 Feb 2008 17:48:55 +0100

yale (1.0-12) unstable; urgency=low

  * Changed the postinst and prerm scripts files according to stardata-common
    policy.
  * Added suggests relationship with stardata-common package and yale-viewer
    virtual package.
  * Changed the extended description field in debian/control file.
  * Updated to Debian Policy 3.6.2.1

 -- Francisco Garcia <franciscomanuel.garcia@hispalinux.es>  Tue, 14 Jun 2005 00:38:26 +0200

yale (1.0-11) unstable; urgency=low

  * New postinst change _really_ fixing the "upgrade when a symlink exists"
    problem.
  * Small changes to the postinst to make it more readable.

 -- Javier Fernandez-Sanguino Pen~a <jfs@computer.org>  Wed,  8 Sep 2004 09:03:49 +0200

yale (1.0-10) unstable; urgency=low

   [ As suggested by Kevin B. McCarty ]
   * Adjusted postinst so that the target file is removed even if it's
     a symlink to avoid the postinst from breaking in some upgrades.
   * Removed trailing / in the OUTFILE definition

 -- Javier Fernandez-Sanguino Pen~a <jfs@computer.org>  Mon,  6 Sep 2004 23:38:43 +0200

yale (1.0-9) unstable; urgency=low

  * Fixed postinst code so that it will behave properly when installing
    spacechart or starplot versions that do _not_ have /var/lib/
    directories.

 -- Javier Fernandez-Sanguino Pen~a <jfs@computer.org>  Sun,  5 Sep 2004 10:38:18 +0200

yale (1.0-8) unstable; urgency=low

  * New maintainer, I'm setting myself as co-maintainer (Closes: #216877)
  * Change perl check to -x check of the gliese2spacechart script
   (so it can be written in other programming languages in the future)
  * Remove unneeded echo from the postinst
  * Simplify removal process and take into account starplot symlinks
    (might appear in some versions)

 -- Javier Fernandez-Sanguino Pen~a <jfs@computer.org>  Fri,  3 Sep 2004 23:46:34 +0200

yale (1.0-7) unstable; urgency=low

  * Changed debian/control section field from non-free/math
    to non-free/science
  * Removed the Recommends field in debian/control
  * Updated to Debian Policy 3.6.1
  * Cleanup the unused calls in rules file
  * Added debhelper in Build-Depends-Indep control
    file field
  * Removed INSTALL from docs file, unneeded in the binary
    file
  * Added the GPL file location to copyright

 -- Francisco Garcia <franciscomanuel.garcia@hispalinux.es>  Fri, 30 Apr 2004 18:13:37 +0200

yale (1.0-6) unstable; urgency=low

  * Added Build-Depends (Closes: #260017)

 -- Javier Fernandez-Sanguino Pen~a <jfs@computer.org>  Sun, 18 Jul 2004 12:06:46 +0200

yale (1.0-5) unstable; urgency=low

  * Fixed postinst so that the 'which' call does not break it
    (should have know that this was going to happen due to the
    set -e)
  * Changed installation to /usr/share/stardata (that's where
    starplot looks it in)

 -- Javier Fernandez-Sanguino Pen~a <jfs@computer.org>  Thu, 29 May 2003 12:58:32 +0200

yale (1.0-4) unstable; urgency=low

  * No longer depends on starplot being available, starconvert is
    only run if it exists.
  * No longer Depends: on starplot but Recommends: it
  * TODO: Move data files from /usr/share/starplot to some common
    place for all astronomical software (/usr/share/star-catalogue?)
  * Updated Standards-Version
  * Removed references to starplot-yale-5 and changed them to just 'yale'

 -- Javier Fernandez-Sanguino Pen~a <jfs@computer.org>  Thu, 29 May 2003 10:31:53 +0200

yale (1.0-3) unstable; urgency=low

  * Modified debian/copyright:
       - add more information on were to obtain this catalogue
       - better description of the copyright issues
  * Added more information on README.Debian and debian/control on
    why it depends on starplot

 -- Javier Fernandez-Sanguino Pen~a <jfs@computer.org>  Wed, 28 May 2003 16:20:31 +0200

yale (1.0-2) unstable; urgency=low

  * Changed package so the starplot data is generated on postinst.

 -- Javier Fernandez-Sanguino Pen~a <jfs@computer.org>  Wed, 15 Aug 2001 17:29:00 +0200

yale (1.0-1) unstable; urgency=low

  * Initial Release.

 -- Javier Fernandez-Sanguino Pen~a <jfs@computer.org>  Tue, 13 Mar 2001 01:28:57 +0100
